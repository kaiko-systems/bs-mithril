type element = Jsx.element

type component<'props> = Jsx.component<'props>
type componentLike<'props, 'return> = Jsx.componentLike<'props, 'return>
external component: componentLike<'props, element> => component<'props> = "%identity"

@val external null: element = "null"
external array: array<element> => element = "%identity"

@module("mithril")
external mithril: ('x, 'props, Jsx.element) => Jsx.element = "default"

let processProps: 'props => ('props, Jsx.element) = %raw(`function(props) {
    let children = undefined;
    if (props !== undefined && props.hasOwnProperty("children")) {
      children = props.children;
      delete props.children;
    }
    return [props, children];
}`)

let attachInitFunction: component<'props> => component<'props> = %raw(`function(make) {
  if (make.init === undefined) {
    make.init = (vnode) => make(vnode.attrs);
  }
  return make.init;
}`)

let jsx = (make: component<'props>, props: 'props): element => {
  let (postProcessed, children) = props->processProps
  mithril(make->attachInitFunction, postProcessed, children)
}

external toDict: 'a => Js.Dict.t<'b> = "%identity"

let jsxs = (component: component<'props>, props: 'props) => jsx(component, props)

let jsxKeyed = (tag: component<'props>, props: 'props, ~key: string, ()) => {
  props->toDict->Js.Dict.set("key", key)
  jsx(tag, props)
}

let jsxsKeyed = (tag: component<'props>, props: 'props, ~key: string, ()) => {
  props->toDict->Js.Dict.set("key", key)
  jsxs(tag, props)
}
