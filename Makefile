PATH := ./node_modules/.bin/:$(PATH)

install node_modules yarn.lock: package.json
	yarn install
	touch node_modules

RESCRIPT_FILES := $(shell find src/ -type f -name '*.res')
compile: $(RESCRIPT_FILES) yarn.lock node_modules
	@if [ -n "$(INSIDE_EMACS)" ]; then \
	    NINJA_ANSI_FORCED=0 rescript build -with-deps; \
	else \
		rescript build -with-deps; \
	fi

clean:
	rm -rf ./node_modules

.PHONY: clean install

publish: compile
	yarn publish --access public
