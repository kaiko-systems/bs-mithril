;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((projectile-project-compilation-cmd . "make compile")))
  (json-mode . ((js-indent-level . 2)))
  (rescript-mode . ((projectile-project-compilation-cmd . "make compile")
                     (indent-tabs-mode . nil))))
