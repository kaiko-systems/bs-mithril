# High level API to [Mithril](https://mithril.js.org/)

TODO


## Overriding React, ReactDOMRe and ReactDOMStyle

This library contains a *replacement* of the modules `React`, `ReactDOMRe` and
`ReactDOMStyle` so you **cannot** use both `bs-mithril` and `rescript-react`
in the same project.

This is because Mithril already has some level of support to React and we bind
those modules to the implementations provided by Mithril itself.
